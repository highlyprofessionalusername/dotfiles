# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=/home/gopal/.dotfiles/zsh/oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"

plugins=(
  git archlinux
)

source $ZSH/oh-my-zsh.sh

# Load all of the config files that end in .zsh
for conf in $HOME/.dotfiles/zsh/conf.d/<00-99>_*.zsh(N); do
    source $conf
done

