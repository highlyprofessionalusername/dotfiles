" ---------------------------------------------------------------------------------------------------------------------
" Housekeeping
  set nomodeline                                 " Disable modelines for security.  
  filetype plugin indent on                      " Enable plugins on a file type basis

  set encoding=utf8                              " Set encoding shown in the terminal
  set fileencoding=utf8                          " Set output encoding for files written

  set backspace=indent,eol,start                 " Make backspace behave

  let g:solarized_termtrans=1                    " Set vim to use the terminal emulator background
  let g:elite_mode=1                             " Disable arrows and make hjkl mandatory

" ---------------------------------------------------------------------------------------------------------------------
" Colours, Highlighting and Themes
  syntax enable                                  " Enable syntax highlighting
  colorscheme solarized                          " Set the color scheme used, solarized or nord
  set background=dark                            " Background colour, dark or light
  set noerrorbells                               " Disable the audio bell
  set novisualbell                               " Disables the visual bell
  set showmatch                                  " Show/highlight matching brackets

" ---------------------------------------------------------------------------------------------------------------------
" Cache and buffers
  set directory=~/.cache/vim                     " Directory to place swap files in
  set nobackup                                   " Disable vim backups
  set nowb                                       " Disable vim write backups
  set noswapfile                                 " Disable vim swap files
  set lazyredraw                                 " Don't update while macros, registers or commands are executed
  set ttyfast                                    " Improved redrawing
  set autoread                                   " Automatically read changes made to open files

" ---------------------------------------------------------------------------------------------------------------------
" UI Configuration
  set number                                     " Show line numbers
  set numberwidth=4                              " Set gutter column width for numbers up to 9999 
  set ruler                                      " Enable the ruler
  
  set cursorline                                 " Highlight current line
  set showcmd                                    " Show command in bottom bar
  set showmode                                   " Show mode (paste/nopaste)

  set wildmenu                                   " Enables a menu 
  set wildmode=list:longest,full                 " Completion on the command line
  set completeopt=longest,menuone                " Completion menu always shows, selects longest and updates as you type

  set foldcolumn=1                               " Add a bit extra margin to the left
  set nofoldenable                               " Disable code folding
  set foldmethod=indent                          " If enabling code folding, set it to indent

  set ttyscroll=3                                " Redraw the screen if scrolling more than N lines
  set scrolloff=1                                " Try to have N lines above/below the current one visible.
  
  set wrap                                       " Set visual line wrapping
  set textwidth=0                                " Set text wrap at N characters, 0 disables
  set wrapmargin=0                               " Set wrap margin at N characters from the right, 0 disables
  set cmdheight=2                                " Height of the command bar

" ---------------------------------------------------------------------------------------------------------------------
" Spaces, tabs and indentation
  set expandtab                                  " TAB keypress inserts spaces in input mode
  set tabstop=4                                  " Width of a tab measured in spaces
  set softtabstop=4                              " Width of a tab in insert mode, measured in spaces
  set shiftwidth=4                               " Size of an indent measured in spaces

  set autoindent                                 " Copy indentation from previous line
  set paste                                      " Set unmodified pasting
  set pastetoggle=<F2>                           " Toggle between paste modess
  set linebreak                                  " Line breaks don't split words.

" ---------------------------------------------------------------------------------------------------------------------
" Searching
  set hlsearch                                   " Highlight search results
  set incsearch                                  " Search while typing
  set ignorecase                                 " Case-insensitive searches
  set smartcase                                  " Only searches containing uppercase are case sensitive.
  set wrapscan                                   " Search wraps around if it hits EOF

" ---------------------------------------------------------------------------------------------------------------------
