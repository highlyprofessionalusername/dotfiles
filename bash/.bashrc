# bashrc configuration v0.1
#!/bin/bash
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

#Load all configuration files found in .dotfiles/bash/conf.d/
for conf in $HOME/.dotfiles/bash/conf.d/[0-99]*.bash; do
    source $conf
done

