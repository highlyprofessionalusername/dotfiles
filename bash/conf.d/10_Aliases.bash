# Commands to be run as root
  alias pacman='sudo pacman'

# Adding some colour
  alias ls='ls --color'
  alias grep='grep --color'

# Command substitutions
  alias nano='vim'
  alias vi='vim'

# Change some dirs
  alias ..='cd ..; ls -l'
  alias ...='cd ../..; ls -l'
  alias ....='cd ../../..; ls -l'
